FROM golang
WORKDIR /gitlab.com/cesar.oliveira2/k8s-study
COPY . .
RUN go build
EXPOSE 3001
CMD ["./k8s-study"]