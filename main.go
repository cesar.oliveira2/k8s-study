package main

import (
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/cesar.oliveira2/k8s-study/src/database"
	"gitlab.com/cesar.oliveira2/k8s-study/src/routes"
)

func main() {
	app := fiber.New()
	database.ConnectDb()

	routes.Routes(app)

	fmt.Println("Testando Minha Aplicação")
	log.Fatal(app.Listen(":3001"))
}
