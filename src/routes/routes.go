package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/cesar.oliveira2/k8s-study/src/controller"
)

func Routes(app *fiber.App) {
	nomes := app.Group("/nomes")
	nomes.Post("/", controller.CreateNome)
	nomes.Get("/", controller.GetAllNomes)
}
