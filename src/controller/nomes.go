package controller

import (
	"encoding/json"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/cesar.oliveira2/k8s-study/src/database"
	"gitlab.com/cesar.oliveira2/k8s-study/src/models"
	responses "gitlab.com/cesar.oliveira2/k8s-study/src/responses/nomes"
	"gitlab.com/cesar.oliveira2/k8s-study/src/utils"
)

func CreateNome(c *fiber.Ctx) error {
	info := utils.Trace()
	db := database.GetDatabase()

	var nome models.Nomes

	if err := c.BodyParser(&nome); err != nil {
		utils.Logger("Erro", info, "Json mal formado")
		return c.Status(400).JSON(err.Error())
	}
	db.Create(&nome)
	response := responses.NomesResponse(nome)

	payload, err := json.Marshal(response)

	if err != nil {
		utils.Logger("Erro", info, "Json mal formado")
		return c.Status(400).JSON(err.Error())
	}

	utils.Logger("Info", info, "Payload"+fmt.Sprintf("%v", string(payload)))

	return c.Status(200).JSON(response)
}

func GetAllNomes(c *fiber.Ctx) error {
	info := utils.Trace()
	db := database.GetDatabase()

	var nomes []models.Nomes

	db.Find(&nomes)

	if len(nomes) == 0 {
		utils.Logger("Info", info, "Banco Vazio")
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Sem nomes no banco", "data": nil})
	}

	return c.JSON(fiber.Map{"status": "success", "message": "Nomes Encontrados", "data": nomes})
}
